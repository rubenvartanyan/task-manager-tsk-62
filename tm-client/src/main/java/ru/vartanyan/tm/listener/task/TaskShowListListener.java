package ru.vartanyan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractTaskListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.endpoint.TaskDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;

import java.util.List;

@Component
public class TaskShowListListener extends AbstractTaskListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    @EventListener(condition = "@taskShowListListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        List<TaskDTO> list;
        list = taskEndpoint.findAllTasks(session);
        int index = 1;
        for (@Nullable final TaskDTO task: list) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
