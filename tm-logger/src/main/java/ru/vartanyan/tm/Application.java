package ru.vartanyan.tm;

import jdk.nashorn.internal.runtime.linker.Bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.IReceiverService;
import ru.vartanyan.tm.configuration.LoggerConfiguration;
import ru.vartanyan.tm.listener.LogMessageListener;
import ru.vartanyan.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final IReceiverService receiverService = context.getBean(ActiveMQConnectionService.class);
        receiverService.receive(context.getBean(LogMessageListener.class));
    }

}
